package de.cas_ual_ty.gci;

import com.google.gson.Gson;
import de.cas_ual_ty.gci.block.BlockGCI;
import de.cas_ual_ty.gci.block.TileEntityMovingLightSource;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.CAPContainerBig;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.CAPContainerBigStorage;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.ContainerBigCapabilityEventHandler;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.ICAPContainerBig;
import de.cas_ual_ty.gci.inventory.capabilities.*;
import de.cas_ual_ty.gci.inventory.inventory.capabilities.*;
import de.cas_ual_ty.gci.item.ItemBlockGCI;
import de.cas_ual_ty.gci.item.ItemFoodGCI;
import de.cas_ual_ty.gci.item.ItemGCI;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.armor.ArmorPiece;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeable;
import de.cas_ual_ty.gci.item.dyeable.ItemDyeableFood;
import de.cas_ual_ty.gci.network.*;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

import static de.cas_ual_ty.gci.GunCusCommand.isAimingServerList;
import static de.cas_ual_ty.gci.GunCusCommand.sendAimingListToEveryPlayer;
import static net.minecraftforge.common.MinecraftForge.EVENT_BUS;

public class Proxy
{
	public void registerItem(IForgeRegistry<Item> registry, ItemGCI item)
	{
		registry.register(item);
	}
	
	public void registerItem(IForgeRegistry<Item> registry, ItemBlockGCI item)
	{
		registry.register(item);
	}

	public void registerItem(IForgeRegistry<Item> registry, ItemDyeableFood item)
	{
		registry.register(item);
	}

	public void registerItem(IForgeRegistry<Item> registry, ItemFoodGCI item)
	{
		registry.register(item);
	}


	// KM
	public void registerItemSimple(IForgeRegistry<Item> registry, ItemGCI item)
	{
		registry.register(item);
	}

	public void registerItemDyeableFood(IForgeRegistry<Item> registry, ItemDyeableFood item)
	{
		registry.register(item);
	}

	public void registerItemFoodGCI(IForgeRegistry<Item> registry, ItemFoodGCI item)
	{
		registry.register(item);
	}

/*	public void registerItemDyeable(IForgeRegistry<Item> registry, ItemDyeable item)
	{
		registry.register(item);
	}*/

	public void registerItem(IForgeRegistry<Item> registry, ArmorPiece item)
	{
		registry.register(item);
	}


	// RP


	public void registerGun(IForgeRegistry<Item> registry, ItemGun gun)
	{
		registry.register(gun);
	}
	
	public void registerBlock(IForgeRegistry<Block> registry, BlockGCI block)
	{
		registry.register(block);
	}
	
	public void preInit(FMLPreInitializationEvent event)
	{
		GunCus.TAB_GUNCUS.shuffleItemStack();
		registerTileEntities();
		NetworkRegistry.INSTANCE.registerGuiHandler(GunCus.instance, new GuiHandlerGCI());
	}
	
	public void init(FMLInitializationEvent event)
	{
		GunCus.channel = NetworkRegistry.INSTANCE.newSimpleChannel(GunCus.MOD_ID);
		GunCus.channel.registerMessage(MessageShoot.MessageHandlerShoot.class, MessageShoot.class, 0, Side.SERVER);
		GunCus.channel.registerMessage(MessageSound.MessageHandlerSound.class, MessageSound.class, 1, Side.CLIENT);
		GunCus.channel.registerMessage(MessageRecoil.MessageHandlerRecoil.class, MessageRecoil.class, 2, Side.CLIENT);
		GunCus.channel.registerMessage(MessagePose.MessageHandlerPose.class, MessagePose.class, 3, Side.CLIENT);
		GunCus.channel.registerMessage(OpenInventoryMessage.OpenInventoryHandler.class, OpenInventoryMessage.class, 4, Side.SERVER);
		GunCus.channel.registerMessage(OpenContainerMessage.OpenContainerHandler.class, OpenContainerMessage.class, 5, Side.SERVER);
		GunCus.channel.registerMessage(SyncInventoryMessage.Handler.class, SyncInventoryMessage.class, 6, Side.CLIENT);
		GunCus.channel.registerMessage(SyncInventoryMessage.Handler.class, SyncInventoryMessage.class, 7, Side.SERVER);
		GunCus.channel.registerMessage(OpenContainerBigMessage.OpenContainerBigHandler.class, OpenContainerBigMessage.class, 8, Side.SERVER);
		EVENT_BUS.register(new EventHandlerServer());



		CapabilityManager.INSTANCE.register(ICAPCustomInventory.class, new CAPCustomInventoryStorage(), CAPCustomInventory.class);
		CapabilityEventHandler.register();

		CapabilityManager.INSTANCE.register(ICAPContainer.class, new CAPContainerStorage(), CAPContainer.class);
		ContainerCapabilityEventHandler.register();

		CapabilityManager.INSTANCE.register(ICAPContainerBig.class, new CAPContainerBigStorage(), CAPContainerBig.class);
		/*ContainerBigCapabilityEventHandler.register();*/

		EntityRegistry.registerModEntity(new ResourceLocation(GunCus.MOD_ID, "bullet"), EntityBullet.class, "bullet", 5000, GunCus.instance, 500, 1, true);
	}

	//KM
	@Mod.EventHandler
	public void startServer(FMLServerStartingEvent event) {
		EVENT_BUS.register(this);

		event.registerServerCommand(new GunCusCommand());

	}
	//RP
	
	public EntityPlayer getServerPlayer(MessageContext ctx)
	{
		return ctx.getServerHandler().player;
	}
	
	public EntityPlayer getClientPlayer(MessageContext ctx)
	{
		return ctx.getServerHandler().player;
	}

	public void registerTileEntities() {
//		System.out.println("Registering tile entities");
		GameRegistry.registerTileEntity(TileEntityMovingLightSource.class, "gci:tileEntityMovingLightSource");
	}
}
