package de.cas_ual_ty.gci.item;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.creativetab.CreativeTabs;

import javax.annotation.Nullable;

public class ItemGCI3D extends ItemGCI {

        private String modelRL;

        public ItemGCI3D(String rl)
        {
            this(rl, null);
        }

        public ItemGCI3D(String rl, @Nullable CreativeTabs tab)
        {
            super(rl, GunCus.KMRP_TAB);
            this.setMaxStackSize(1);
        }



}
