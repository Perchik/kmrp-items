package de.cas_ual_ty.gci.item.dyeable;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;

import javax.annotation.Nullable;

public class ItemDyeableFood extends ItemFood {
    private static final String NBT_COLOR = "itemDyeableColor";
    private String modelRL;

    public ItemDyeableFood(String rl)
    {
        this(rl, null);
    }

    public ItemDyeableFood(String rl, @Nullable CreativeTabs tab)
    {
        super(0, 0, false);
        this.setUnlocalizedName(GunCus.MOD_ID + ":" + rl);
        this.setRegistryName(GunCus.MOD_ID + ":" + rl);
        this.modelRL = rl;

        if(tab != null)
        {
            this.setCreativeTab(tab);
        }
        else
        {
            this.setCreativeTab(GunCus.KMRP_TAB);
        }
    }

    public String getModelRL()
    {
        return this.modelRL;
    }

    public static int getItemDyeableColor(ItemStack stack) {
        if (stack.getTagCompound() != null) {
            if (stack.getTagCompound().hasKey(NBT_COLOR)) {
                return stack.getTagCompound().getInteger(NBT_COLOR);
            }
        }
        return -1;
    }

    public static void setItemDyeableColor(ItemStack stack, int color) {
        NBTTagInt nbtTagInt = new NBTTagInt(color);
        if (stack.getTagCompound() == null) {
            stack.setTagCompound(new NBTTagCompound());
        }
        stack.getTagCompound().setTag(NBT_COLOR, nbtTagInt);
    }

    public static int getItemColor(ItemStack stack, int tintIndex) {
        if (tintIndex == 0) {
            return getItemDyeableColor(stack);
        }
        return -1;
    }
}
