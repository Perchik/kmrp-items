package de.cas_ual_ty.gci.item.dyeable;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.item.ItemGCI;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.util.NonNullList;

import javax.annotation.Nullable;
import java.awt.*;

public class ItemDyeable extends ItemGCI {
    private static final String NBT_COLOR = "itemDyeableColor";

    private String modelRL;

    public ItemDyeable(String rl)
    {
        this(rl, null);
    }

    public ItemDyeable(String rl, @Nullable CreativeTabs tab)
    {
        super(rl, GunCus.KMRP_TAB);
    }


    public static int getItemDyeableColor(ItemStack stack) {
        if (stack.getTagCompound() != null) {
            if (stack.getTagCompound().hasKey(NBT_COLOR)) {
                return stack.getTagCompound().getInteger(NBT_COLOR);
            }
        }
        return -1;
    }

    public static void setItemDyeableColor(ItemStack stack, int color) {
        NBTTagInt nbtTagInt = new NBTTagInt(color);
        if (stack.getTagCompound() == null) {
            stack.setTagCompound(new NBTTagCompound());
        }
        stack.getTagCompound().setTag(NBT_COLOR, nbtTagInt);
    }

    public static int getItemColor(ItemStack stack, int tintIndex) {
        if (tintIndex == 0) {
            return getItemDyeableColor(stack);
        }
        return -1;
    }


/*    @Override
    public void getSubItems (CreativeTabs tab, NonNullList<ItemStack> items) {
        if (isInCreativeTab(tab)) {
            for (EnumDyeColor color : EnumDyeColor.values()) {
                ItemStack is = new ItemStack(this);
                setItemDyeableColor(is, color.getColorValue());
            }
        }
        super.getSubItems(tab, items);
    }*/
}
