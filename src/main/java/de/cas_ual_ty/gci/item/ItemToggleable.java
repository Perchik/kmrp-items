package de.cas_ual_ty.gci.item;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;

import javax.annotation.Nullable;

public class ItemToggleable extends ItemGCI3D {



    private String modelRL;

    public ItemToggleable(String rl)
    {
        this(rl, null);
    }

    public ItemToggleable(String rl, @Nullable CreativeTabs tab)
    {
        super(rl, GunCus.KMRP_TAB);
        this.setMaxStackSize(1);
    }

    //resource
    public String toggleableTo = null;


    public ItemToggleable setToggleableTo(String id) {
        this.toggleableTo = id;
        return this;
    }


    public String getToggleableTo() {
        return this.toggleableTo;
    }

}