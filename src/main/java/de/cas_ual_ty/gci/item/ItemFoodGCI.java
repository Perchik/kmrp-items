package de.cas_ual_ty.gci.item;

import de.cas_ual_ty.gci.GunCus;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;

import javax.annotation.Nullable;

public class ItemFoodGCI extends ItemFood {
    private String modelRL;

    public ItemFoodGCI(String rl)
    {
        this(rl, null);
    }

    public ItemFoodGCI(String rl, @Nullable CreativeTabs tab)
    {
        super(0, 0, false);
        this.setUnlocalizedName(GunCus.MOD_ID + ":" + rl);
        this.setRegistryName(GunCus.MOD_ID + ":" + rl);
        this.modelRL = rl;

        if(tab != null)
        {
            this.setCreativeTab(tab);
        }
        else
        {
            this.setCreativeTab(GunCus.KMRP_TAB);
        }
    }

    public String getModelRL()
    {
        return this.modelRL;
    }
}
