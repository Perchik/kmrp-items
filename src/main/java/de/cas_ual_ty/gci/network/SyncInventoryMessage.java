package de.cas_ual_ty.gci.network;

import de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.management.PlayerList;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Arrays;

public class SyncInventoryMessage implements IMessage {

    public NBTTagCompound nbt;
    public int playerId;

    public SyncInventoryMessage() {}

    public SyncInventoryMessage(NBTTagCompound compound, int id) {
        nbt = compound;
        playerId = id;
    }

    @Override
    public void toBytes(ByteBuf buffer) {
        ByteBufUtils.writeTag(buffer, nbt);
        buffer.writeInt(playerId);
    }

    @Override
    public void fromBytes(ByteBuf buffer) {
        nbt = ByteBufUtils.readTag(buffer);
        playerId = buffer.readInt();
    }

    public static class Handler implements IMessageHandler<SyncInventoryMessage, IMessage>
    {
        @Override
        @SideOnly(Side.CLIENT)
        public IMessage onMessage(SyncInventoryMessage message, MessageContext ctx) {
            try {
                Minecraft.getMinecraft().addScheduledTask(new Runnable(){ public void run() {
                    World world = FMLClientHandler.instance().getClient().world;
                    if (world==null) return;
                    Entity p = world.getEntityByID(message.playerId); //TODO sus
//                    if (world.getMinecraftServer().getPlayerList().getPlayerByUsername(p.getName()) == null) {
//                        world.removeEntity(p);
//                        return;
//                    }
                    if (p !=null && p instanceof EntityPlayer) {
                        CAPCustomInventoryProvider.INVENTORY_CAP.getStorage().readNBT(CAPCustomInventoryProvider.INVENTORY_CAP, p.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null), null, message.nbt);
                    }
                }});
            } catch (NullPointerException e) {
                System.out.println("null");
            }
            return null;
        }
    }
}