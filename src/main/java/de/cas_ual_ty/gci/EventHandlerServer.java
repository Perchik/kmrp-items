package de.cas_ual_ty.gci;

import com.google.gson.Gson;
import de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider;
import de.cas_ual_ty.gci.network.MessagePose;
import de.cas_ual_ty.gci.network.SyncInventoryMessage;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import static de.cas_ual_ty.gci.GunCusCommand.isAimingServerList;
import static de.cas_ual_ty.gci.GunCusCommand.sendAimingListToEveryPlayer;

public class EventHandlerServer {
    @SideOnly(Side.SERVER)
    @SubscribeEvent(priority = EventPriority.NORMAL)
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayer player = event.player;
        /*System.out.println(player.getName() + " logged in");*/
/*        Gson gson = new Gson();
        String jsonList = gson.toJson(isAimingServerList);
        GunCus.channel.sendTo(new MessagePose(jsonList), (EntityPlayerMP) player);*/
        sendAimingListToEveryPlayer();
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent(priority = EventPriority.NORMAL)
    public void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        EntityPlayer player = event.player;
        /*System.out.println(player.getName() + " logged out");*/
        if (isAimingServerList.contains(player.getName())) {
            isAimingServerList.remove(player.getName());
            sendAimingListToEveryPlayer();
        }
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent
    public void onStartTracking(net.minecraftforge.event.entity.player.PlayerEvent.StartTracking event) {
        Entity target = event.getTarget();
        if (target instanceof EntityPlayerMP) {
//            try {
//                if (target.world.getMinecraftServer().getPlayerList().getPlayerByUsername(target.getName()) == null) {
//                    target.world.removeEntity(target);
//                }
//            } catch (Exception ignored) {
//
//            }
            NBTBase nbt = CAPCustomInventoryProvider.INVENTORY_CAP.getStorage().writeNBT(CAPCustomInventoryProvider.INVENTORY_CAP, target.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null), null);
            GunCus.channel.sendToAll(new SyncInventoryMessage((NBTTagCompound) nbt, target.getEntityId()));
            /*System.out.println("test1");*/

        }
    }

    @SideOnly(Side.SERVER)
    @SubscribeEvent
    public void playerJoin(EntityJoinWorldEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP) entity;
            NBTBase nbt = CAPCustomInventoryProvider.INVENTORY_CAP.getStorage().writeNBT(CAPCustomInventoryProvider.INVENTORY_CAP, player.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null), null);
            GunCus.channel.sendToAll(new SyncInventoryMessage((NBTTagCompound) nbt, player.getEntityId()));
            /*System.out.println("test2");*/

        }
    }
}
