package de.cas_ual_ty.gci.client;

import javax.annotation.Nullable;

import com.google.gson.Gson;
import de.cas_ual_ty.gci.GunCusCommand;
import de.cas_ual_ty.gci.block.BlockMovingLightSource;
import de.cas_ual_ty.gci.client.render.RenderFactoryGCI;
import de.cas_ual_ty.gci.client.render.layers.BackLayer;
import de.cas_ual_ty.gci.inventory.CustomInventory;
import de.cas_ual_ty.gci.inventory.capabilities.CAPCustomInventoryProvider;
import de.cas_ual_ty.gci.inventory.capabilities.ICAPCustomInventory;
import de.cas_ual_ty.gci.item.ItemGCI3D;
import de.cas_ual_ty.gci.item.attachment.*;
import de.cas_ual_ty.gci.network.*;
import net.minecraft.block.Block;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.block.model.IBakedModel;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.client.renderer.block.model.ItemModelGenerator;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.PotionTypes;
import net.minecraft.item.ItemShield;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionType;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.*;
import net.minecraftforge.client.event.*;
import net.minecraftforge.client.model.BakedItemModel;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;
import net.minecraftforge.client.event.RenderLivingEvent;

import de.cas_ual_ty.gci.EntityBullet;
import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.client.render.GuiSight;
import de.cas_ual_ty.gci.item.ItemGun;
import de.cas_ual_ty.gci.item.attachment.Auxiliary.Laser;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static com.google.gson.internal.bind.TypeAdapters.UUID;
import static de.cas_ual_ty.gci.GunCusCommand.isAimingServerList;
import static de.cas_ual_ty.gci.GunCusCommand.sendAimingListToEveryPlayer;
import static de.cas_ual_ty.gci.client.ProxyClient.mpmdir;
import static net.minecraft.command.CommandBase.getPlayer;


public class EventHandlerClient
{
	public static ArrayList<String> isAimingClientList = new ArrayList<String>();
	public static final GuiSight GUI_SIGHT = new GuiSight();
	private static final ModelRendererTransformationHelper TRANSFORMATION_HELPER = new ModelRendererTransformationHelper();
	public static NBTTagCompound TESTING1 = null;

	public static NBTTagCompound loadPlayerData1(java.util.UUID id){
		String filename = id.toString();
		if(filename.isEmpty())
			filename = "noplayername";
		filename += ".dat";

		try {
			File file = new File(mpmdir, filename);
			if(!file.exists()){
				return null;
			}
			return CompressedStreamTools.readCompressed(new FileInputStream(file));
		} catch (Exception e) {

		}
		try {
			File file = new File(mpmdir, filename+"_old");
			if(!file.exists()){
				return null;
			}
			return CompressedStreamTools.readCompressed(new FileInputStream(file));

		} catch (Exception e) {

		}
		return null;
	}
/*	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public void onEvent(RenderPlayerEvent.Pre event)
	{
		EntityPlayer entityPlayer = event.getEntityPlayer();
		if(!entityPlayer.isSprinting() && !entityPlayer.isSneaking())
		{
			if(entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun)
			{
				event.getRenderer().getMainModel().bipedRightArm.isHidden = true;
                event.getRenderer().getMainModel().bipedRightArmwear.isHidden = true;

				if(entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun || entityPlayer.getHeldItemOffhand().isEmpty())
				{
					event.getRenderer().getMainModel().bipedLeftArm.isHidden = true;
                    event.getRenderer().getMainModel().bipedLeftArmwear.isHidden = true;
				}
			}
			else if(entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun)
			{
				event.getRenderer().getMainModel().bipedLeftArm.isHidden = true;
                event.getRenderer().getMainModel().bipedLeftArmwear.isHidden = true;
			}
		}
	}
	
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public void onEvent(RenderPlayerEvent.Post event)
	{
		EntityPlayer entityPlayer = event.getEntityPlayer();

		if(!entityPlayer.isSprinting() && !entityPlayer.isSneaking())
		{
			if(entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun)
			{
				Minecraft.getMinecraft().getTextureManager().bindTexture(event.getRenderer().getEntityTexture((AbstractClientPlayer) entityPlayer));

				if(entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun)
				{
					event.getRenderer().getMainModel().bipedRightArm.isHidden = false;
					EventHandlerClient.TRANSFORMATION_HELPER.set(
							-MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							20,
							-MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							(float) Math.toRadians(90D),
							(float) -Math.toRadians(entityPlayer.renderYawOffset),
							0
							).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedRightArm, 0.0625F);

                    event.getRenderer().getMainModel().bipedRightArmwear.isHidden = false;
                    EventHandlerClient.TRANSFORMATION_HELPER.set(
                            -MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            20,
                            -MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            (float) Math.toRadians(90D),
                            (float) -Math.toRadians(entityPlayer.renderYawOffset),
                            0
                    ).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedRightArmwear, 0.0625F);

					event.getRenderer().getMainModel().bipedLeftArm.isHidden = false;
					EventHandlerClient.TRANSFORMATION_HELPER.set(
							MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							20,
							MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							(float) Math.toRadians(90D),
							(float) -Math.toRadians(entityPlayer.renderYawOffset),
							0
							).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedLeftArm, 0.0625F);

                    event.getRenderer().getMainModel().bipedLeftArmwear.isHidden = false;
                    EventHandlerClient.TRANSFORMATION_HELPER.set(
                            -MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            20,
                            -MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            (float) Math.toRadians(90D),
                            (float) -Math.toRadians(entityPlayer.renderYawOffset),
                            0
                    ).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedLeftArmwear, 0.0625F);
				}
				else if(!entityPlayer.getHeldItemOffhand().isEmpty())
				{
					event.getRenderer().getMainModel().bipedRightArm.isHidden = false;
					EventHandlerClient.TRANSFORMATION_HELPER.set(
							-MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							20,
							-MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							(float) Math.toRadians(90D),
							(float) -Math.toRadians(entityPlayer.renderYawOffset),
							0
							).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedRightArm, 0.0625F);

                    event.getRenderer().getMainModel().bipedRightArmwear.isHidden = false;
                    EventHandlerClient.TRANSFORMATION_HELPER.set(
                            -MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            20,
                            -MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            (float) Math.toRadians(90D),
                            (float) -Math.toRadians(entityPlayer.renderYawOffset),
                            0
                    ).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedRightArmwear, 0.0625F);
				}
				else
				{
					event.getRenderer().getMainModel().bipedRightArm.isHidden = false;
					EventHandlerClient.TRANSFORMATION_HELPER.set(
							-MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							20,GunCus.channel.sendToServer(new OpenContainerMessage());
							-MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							(float) Math.toRadians(90D),
							(float) -Math.toRadians(entityPlayer.renderYawOffset),
							0
							).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedRightArm, 0.0625F);

                    event.getRenderer().getMainModel().bipedRightArmwear.isHidden = false;
                    EventHandlerClient.TRANSFORMATION_HELPER.set(
                            -MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            20,
                            -MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            (float) Math.toRadians(90D),
                            (float) -Math.toRadians(entityPlayer.renderYawOffset),
                            0
                    ).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedRightArmwear, 0.0625F);

					event.getRenderer().getMainModel().bipedLeftArm.isHidden = false;
					EventHandlerClient.TRANSFORMATION_HELPER.set(
							MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							20,
							MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
							(float) Math.toRadians(90D),
							(float) -Math.toRadians(entityPlayer.renderYawOffset + 45D),
							0
							).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedLeftArm, 0.0625F);

                    event.getRenderer().getMainModel().bipedLeftArmwear.isHidden = false;
                    EventHandlerClient.TRANSFORMATION_HELPER.set(
                            -MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            20,
                            -MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                            (float) Math.toRadians(90D),
                            (float) -Math.toRadians(entityPlayer.renderYawOffset),
                            0
                    ).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedLeftArmwear, 0.0625F);
				}
			}
			else if(entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun)
			{
				Minecraft.getMinecraft().getTextureManager().bindTexture(event.getRenderer().getEntityTexture((AbstractClientPlayer) entityPlayer));

				event.getRenderer().getMainModel().bipedLeftArm.isHidden = false;
				EventHandlerClient.TRANSFORMATION_HELPER.set(
						MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
						20,
						MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
						(float) Math.toRadians(90D),
						(float) -Math.toRadians(entityPlayer.renderYawOffset),
						0
						).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedLeftArm, 0.0625F);

                event.getRenderer().getMainModel().bipedLeftArmwear.isHidden = false;
                EventHandlerClient.TRANSFORMATION_HELPER.set(
                        -MathHelper.cos((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                        20,
                        -MathHelper.sin((float) Math.toRadians(entityPlayer.renderYawOffset)) * 4.5F,
                        (float) Math.toRadians(90D),
                        (float) -Math.toRadians(entityPlayer.renderYawOffset),
                        0
                ).transformAndRender(entityPlayer, event.getRenderer().getMainModel().bipedLeftArmwear, 0.0625F);
			}
		}
	}*/

/*	@SubscribeEvent
	public void grembipi1(RenderPlayerEvent.Specials.Post event) {
		ICAPCustomInventory cap = event.getEntityPlayer().getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
		CustomInventory inv = cap.getInventory();
		ItemStack is = inv.getStackInSlot(0);

		if (is != null && is.getItem() instanceof ItemGun) {
			GlStateManager.pushMatrix();
			event.getRenderer().getMainModel().bipedHead.postRender(0.0625f);
			//GlStateManager.translate(0.5f, -0.5F, -0.5F); //менять x чтобы двигать левее-правее
			//GlStateManager.rotate(-55F, 1, 0, 0);
			//GlStateManager.pushMatrix();
			ItemRenderer renderer = Minecraft.getMinecraft().getItemRenderer();
			renderer.renderItem(event.getEntityPlayer(), event.getEntityPlayer().getHeldItemMainhand(), ItemCameraTransforms.TransformType.FIXED);

		}
	}*/

	@SubscribeEvent
	public void onRightClickItem(PlayerInteractEvent.RightClickItem event) {
		ItemStack is = event.getItemStack();
		if (is.getTagCompound() != null) {
			if (is.getTagCompound().hasKey("bigcontainer")) {
				GunCus.channel.sendToServer(new OpenContainerBigMessage());
			} else if (is.getTagCompound().hasKey("mediumcontainer")) {
				GunCus.channel.sendToServer(new OpenContainerMessage());
			}
		}
	}

	// Удалять кривые сообщения от стороннего мода
	@SubscribeEvent
	public void onChatEvent(ClientChatReceivedEvent event) {
		if (event.getMessage().getUnformattedText().equals("Reeves's Furniture Mod (v.3.1)") || event.getMessage().getUnformattedText().equals("Thank you for download!") || event.getMessage().getUnformattedText().equals("Happy Halloween!")) event.setCanceled(true);
		if (event.getMessage().getUnformattedText().equals("Вы наступили на мину. Зови ГМ.")) Minecraft.getMinecraft().player.sendChatMessage("- <@&298615352642895882> я наступил на мину по следующим координатам: x:" +  Minecraft.getMinecraft().player.getPosition().getX() + " y:" + Minecraft.getMinecraft().player.getPosition().getY() + " z:" + Minecraft.getMinecraft().player.getPosition().getZ());

	}

	//Вроде не работает но пусть будет пока
	@SubscribeEvent
	public void onStartTracking(net.minecraftforge.event.entity.player.PlayerEvent.StartTracking event) {
		Entity target = event.getTarget();
		if (target instanceof EntityPlayerMP) {

			NBTBase nbt = CAPCustomInventoryProvider.INVENTORY_CAP.getStorage().writeNBT(CAPCustomInventoryProvider.INVENTORY_CAP, target.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null), null);
			GunCus.channel.sendToAll(new SyncInventoryMessage((NBTTagCompound) nbt, target.getEntityId()));

		}
	}

	@SubscribeEvent
	public void onRenderLivingEventPre2(RenderLivingEvent.Pre event) {
		
	}

	@SubscribeEvent
	public void entJoin(EntityJoinWorldEvent event) {
		Entity entity = event.getEntity();
		//Скорее всего не нужно
		if (entity instanceof EntityPlayerMP) {
			EntityPlayerMP player = (EntityPlayerMP) entity;
			NBTBase nbt = CAPCustomInventoryProvider.INVENTORY_CAP.getStorage().writeNBT(CAPCustomInventoryProvider.INVENTORY_CAP, player.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null), null);
			GunCus.channel.sendToAll(new SyncInventoryMessage((NBTTagCompound) nbt, player.getEntityId()));

		}		//
	}


	@SubscribeEvent
	public void clientJoin(FMLNetworkEvent.ClientConnectedToServerEvent event) {
		TESTING1 = null;//
	}

//NEED REWORKING
	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = false)
	public void onRenderLivingEventPre(RenderLivingEvent.Pre event) {
		if (event.getEntity() instanceof EntityPlayer) {
			EntityPlayer entityPlayer = (EntityPlayer) event.getEntity();
			ModelBase mdl = event.getRenderer().getMainModel();
			if (mdl instanceof ModelPlayer) {
				ModelPlayer model = (ModelPlayer) mdl;
				//System.out.println(entityPlayer.getCapability(PoseProvider.IS_AIMING, null).isAiming());
				if (isAimingClientList.contains(entityPlayer.getName())) {
					// in aiming list
					if (entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun) {
						// At least right hand has a gun
						if (entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun) {
							//If every hand has a gun
							model.rightArmPose = ModelBiped.ArmPose.BOW_AND_ARROW;
							model.leftArmPose = ModelBiped.ArmPose.BOW_AND_ARROW;
						} else {
							//If only right hand has a gun
							model.rightArmPose = ModelBiped.ArmPose.BOW_AND_ARROW;
							if (entityPlayer.getHeldItemOffhand().getItem() instanceof ItemShield) {
								//If right hand has a gun and left hand has a shield
								model.leftArmPose = ModelBiped.ArmPose.BLOCK;
								entityPlayer.getHeldItemOffhand().useItemRightClick(entityPlayer.getEntityWorld(), entityPlayer, EnumHand.OFF_HAND);
							}
						}
					} else if (entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun) {
						// Left hand has a gun
						model.leftArmPose = ModelBiped.ArmPose.BOW_AND_ARROW;
						if (entityPlayer.getHeldItemMainhand().getItem() instanceof ItemShield) {
							// Right hand has a shield
							model.rightArmPose = ModelBiped.ArmPose.BLOCK;
						}
					} else {
						if (entityPlayer.getHeldItemMainhand().getItem() instanceof ItemShield) {
							// No guns, but right hand has a shield
							model.rightArmPose = ModelBiped.ArmPose.BLOCK;
						} else {
							// No guns but player is aiming anyway.
							model.rightArmPose = ModelBiped.ArmPose.BOW_AND_ARROW;
						}
						if (entityPlayer.getHeldItemOffhand().getItem() instanceof ItemShield) {
							// Left hand has a shield, no guns
							model.leftArmPose = ModelBiped.ArmPose.BLOCK;
							//entityPlayer.getHeldItemOffhand().useItemRightClick(entityPlayer.getEntityWorld(), entityPlayer, EnumHand.OFF_HAND);
						} else {
							// No guns but player is aiming anyway.
							model.leftArmPose = ModelBiped.ArmPose.BOW_AND_ARROW;
						}
					}
				} else {
					// not in aiming list
					if (entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun && entityPlayer.getHeldItemOffhand().isEmpty()) {
						// Only right hand has a gun and player isn't aiming
						//model.rightArmPose = ModelBiped.ArmPose.BLOCK;
					} else if (entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun && entityPlayer.getHeldItemMainhand().isEmpty()) {
						// Only left hand has a gun and player isn't aiming
						//model.leftArmPose = ModelBiped.ArmPose.BLOCK;
					}
				}

			}
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void renderSpecificHandEvent(RenderSpecificHandEvent event) {
		EntityPlayer entityPlayer = Minecraft.getMinecraft().player;
		if (entityPlayer.getHeldItem(event.getHand()).getItem() instanceof ItemGun && !isAimingClientList.contains(entityPlayer.getName())) {
			if (!entityPlayer.getHeldItem(event.getHand()).getItem().equals(GunCus.SWORD_MODULAR) && !entityPlayer.getHeldItem(event.getHand()).getItem().equals(GunCus.SWORD_KATANA)) {
				event.setCanceled(true);
				GlStateManager.pushMatrix();
				if (event.getHand() == EnumHand.MAIN_HAND) {
					GlStateManager.translate(0.3f, -0.5F, -0.6F); //менять x чтобы двигать левее-правее
					GlStateManager.rotate(-90F, 0, 1, 0);
					GlStateManager.rotate(90F, 0, 0, 1);
				} else if (event.getHand() == EnumHand.OFF_HAND) {
					GlStateManager.translate(-0.5f, -0.5F, -0.6F); //менять x чтобы двигать левее-правее
					GlStateManager.rotate(-90F, 0, 1, 0);
					GlStateManager.rotate(90F, 0, 0, 1);
				}
			ItemRenderer renderer = Minecraft.getMinecraft().getItemRenderer();
			renderer.renderItem(entityPlayer, entityPlayer.getHeldItem(event.getHand()), ItemCameraTransforms.TransformType.FIXED);

			GlStateManager.popMatrix();
			}
		}
	}
/*	public static IPoseHandler getHandler(Entity entity) {

		if (entity.hasCapability(IS_AIMING, EnumFacing.DOWN))
			return entity.getCapability(IS_AIMING, EnumFacing.DOWN);
		return null;
	}*/

	private int tabCounter = 0;
	private static final int TAB_ITEM_INTERVAL = 20;


	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void clientTick(ClientTickEvent event)
	{
		if(event.phase == Phase.START) {
			//стрельба
/*			if (Minecraft.getMinecraft().gameSettings.keyBindAttack.isKeyDown() && Minecraft.getMinecraft().player != null) {
				boolean aiming = false;
				EntityPlayer entityPlayer = Minecraft.getMinecraft().player;

				if (Minecraft.getMinecraft().gameSettings.keyBindUseItem.isKeyDown() && !entityPlayer.isSprinting()) {

					if (entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun && entityPlayer.getHeldItemOffhand().isEmpty()) {
						ItemStack itemStack = entityPlayer.getHeldItemMainhand();
						ItemGun gun = (ItemGun) itemStack.getItem();

						if (gun.getCanAim(itemStack)) {
							Optic optic = gun.<Optic>getAttachmentCalled(itemStack, EnumAttachmentType.OPTIC.getSlot());

							if (optic != null && optic.canAim()) {
								aiming = true;
							}
						}
					}
				}

				GunCus.channel.sendToServer(new MessageShoot(aiming, (!entityPlayer.onGround || (entityPlayer.motionX != 0) || (entityPlayer.motionY != 0))));
			}*/

			++this.tabCounter;

			if (this.tabCounter >= EventHandlerClient.TAB_ITEM_INTERVAL) {
				GunCus.TAB_GUNCUS.shuffleItemStack();

				this.tabCounter = 0;
			}
		}

	}

	@SuppressWarnings("deprecation")
	@SubscribeEvent(priority=EventPriority.NORMAL, receiveCanceled=true)
	public void onEvent(PlayerTickEvent event)
	{
//		System.out.println("PlayerTickEvent");
		if (event.phase == TickEvent.Phase.START)
		{
//			System.out.println("PlayerTickEventWorking");
			// check if player is holding a light source
			if (BlockMovingLightSource.isHoldingLightItem(event.player))
			{
//                	// DEBUG
//				System.out.println("Holding torch");

				// determine player position
				int blockX = MathHelper.floor(event.player.posX);
				int blockY = MathHelper.floor(event.player.posY-0.2D - event.player.getYOffset());
				int blockZ = MathHelper.floor(event.player.posZ);

				// place light where there is space to do so
				BlockPos blockLocation = new BlockPos(blockX, blockY, blockZ).up();
				Block blockAtLocation = event.player.world.getBlockState(blockLocation).getBlock();
//                    // DEBUG
//				System.out.println("Block at player position is "+event.player.world.getBlockState(blockLocation).getBlock());
				if (blockAtLocation == Blocks.AIR)
				{
//                    	// DEBUG
//					System.out.println("There is space at player location "+blockLocation+" to place block");

					// there is space to create moving light source block
					event.player.world.setBlockState(
							blockLocation,
							BlockMovingLightSource.lightBlockToPlace(event.player).getDefaultState()
					);
				}


			}
		}
	}

	private Integer rangeF = null;

	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void renderGameOverlayText(RenderGameOverlayEvent.Text event) {
		if (FMLClientHandler.instance().getClient().player.isCreative())
			return;
		Iterator<String> it = event.getLeft().listIterator();
		while (it.hasNext()) {
			String value = it.next();
			if (value != null && (value.startsWith("x:") || value.startsWith("y:") || value.startsWith("z:") || value.startsWith("XYZ:") || value.startsWith("Looking at:") || value.startsWith("Block:") || value.startsWith("Chunk:") || value.startsWith("Facing:")))
				it.remove();
		}
	}


	@SideOnly(Side.CLIENT)
	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void renderGameOverlay(RenderGameOverlayEvent event)
	{
		if(event.getType() == ElementType.CROSSHAIRS && Minecraft.getMinecraft().player != null)
		{
			EntityPlayer entityPlayer = Minecraft.getMinecraft().player;
			Optic optic = null;
			
			if(entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun || entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun)
			{
				if(entityPlayer.getHeldItemOffhand().isEmpty())
				{
					ItemStack itemStack = entityPlayer.getHeldItemMainhand();
					ItemGun gun = (ItemGun) itemStack.getItem();
					
					if(gun.getCanAim(itemStack))
					{
						optic = gun.<Optic>getAttachmentCalled(itemStack, EnumAttachmentType.OPTIC.getSlot());
					}
				}
				
				event.setCanceled(true);
			}
			else if(entityPlayer.getHeldItemMainhand().getItem() instanceof Optic)
			{
				ItemStack itemStack = entityPlayer.getHeldItemMainhand();
				optic = (Optic) itemStack.getItem();
			}
			
			if(!entityPlayer.isSprinting() && Minecraft.getMinecraft().gameSettings.keyBindUseItem.isKeyDown() && optic != null && optic.canAim())
			{
				EventHandlerClient.GUI_SIGHT.draw(optic, event.getResolution());
				
				if(!event.isCanceled())
				{
					event.setCanceled(true);
				}
			}
		}

		if (rangeF != null) {
			FontRenderer fRender = Minecraft.getMinecraft().fontRenderer;
			int posX = event.getResolution().getScaledWidth() / 2 - fRender.getStringWidth(rangeF.toString());
			int posY = event.getResolution().getScaledHeight() / 3 * 2;
			fRender.drawStringWithShadow(rangeF.toString(), posX, posY, 0xFFFFFF);
			rangeF = null;
		}
	}

	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void fovUpdate(FOVUpdateEvent event)
	{

		if(Minecraft.getMinecraft().player != null && Minecraft.getMinecraft().gameSettings.keyBindUseItem.isKeyDown())
		{
			EntityPlayer entityPlayer = Minecraft.getMinecraft().player;
			
			if(!entityPlayer.isSprinting() && Minecraft.getMinecraft().gameSettings.keyBindUseItem.isKeyDown())
			{
				Optic optic = null;
				float modifier = 1F;
				float extra = 0F;
				
				if(entityPlayer.getHeldItemMainhand().getItem() instanceof ItemGun || entityPlayer.getHeldItemOffhand().getItem() instanceof ItemGun)
				{
					if(entityPlayer.getHeldItemOffhand().isEmpty())
					{
						ItemStack itemStack = entityPlayer.getHeldItemMainhand();
						ItemGun gun = (ItemGun) itemStack.getItem();
						
						if(gun.getCanAim(itemStack))
						{
							optic = gun.<Optic>getAttachmentCalled(itemStack, EnumAttachmentType.OPTIC.getSlot());
						}
						
						if(optic != null && gun.isAccessoryTurnedOn(itemStack) && gun.getAttachment(itemStack, EnumAttachmentType.ACCESSORY.getSlot()) != null)
						{
							Accessory accessory = gun.<Accessory>getAttachmentCalled(itemStack, EnumAttachmentType.ACCESSORY.getSlot());
							
							if(optic.isCompatibleWithMagnifiers())
							{
								modifier = accessory.getZoomModifier();
							}
							
							if(optic.isCompatibleWithExtraZoom())
							{
								extra = accessory.getExtraZoom();
							}
						}
					}
				}
				else if(entityPlayer.getHeldItemMainhand().getItem() instanceof Optic)
				{
					ItemStack itemStack = entityPlayer.getHeldItemMainhand();
					optic = (Optic) itemStack.getItem();
				}
				
				if(optic != null && optic.canAim())
				{
					event.setNewfov(EventHandlerClient.calculateFov(optic.getZoom() * modifier + 0.1F, event.getFov()));
					if (optic.getOpticType() == Optic.EnumOpticType.NIGHT_VISION) {
						//доделать
						//event.getEntity().addPotionEffect(PotionTypes.NIGHT_VISION.getEffects().get(0));
					}
				}
			}
		}
	}
	
	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void renderWorldLastEvent(RenderWorldLastEvent event)
	{
		if(Minecraft.getMinecraft().world != null)
		{
			World world = Minecraft.getMinecraft().world;
			EntityPlayer clientPlayer = Minecraft.getMinecraft().player;
			//
			//MinecraftServer ms = FMLCommonHandler.instance().getMinecraftServerInstance();
			//
			ItemStack itemStack;
			ItemGun gun;
			Auxiliary auxiliary = null;
			Underbarrel underbarrel = null;
			Optic optic = null;
			Ammo rightside = null;
			Laser laser = null;
			Underbarrel.Laser laser1 = null;
			Optic.Laser laser2 = null;
			Ammo.Laser laser3 = null;
			//Integer range = null;
			String title = null;
			
			Vec3d start;
			Vec3d end;
			Vec3d subtract = new Vec3d(clientPlayer.posX, clientPlayer.posY, clientPlayer.posZ);
			RayTraceResult resultBlock;
			RayTraceResult resultEntity;
			
			double rangeBlockSq;
			double rangeEntitySq;
			
			boolean renderPoint = false;
			boolean rangeFinder = false;
			
			Tessellator tessellator = Tessellator.getInstance();
			BufferBuilder b = tessellator.getBuffer();
			//System.out.println("Hello world!");
			for(EntityPlayer entityPlayer : world.playerEntities)
			{
				boolean rfHolder = entityPlayer == clientPlayer;
				if(!entityPlayer.isDead && isAimingClientList.contains(entityPlayer.getName()))
				{
					//					subtract = new Vec3d(entityPlayer.posX - entityPlayer.motionX * event.getPartialTicks(), entityPlayer.posY /*+ entityPlayer.getEyeHeight()*/ - (entityPlayer.onGround ? 0 : entityPlayer.motionY * event.getPartialTicks()), entityPlayer.posZ - entityPlayer.motionZ * event.getPartialTicks());
					//					subtract = new Vec3d(entityPlayer.posX, entityPlayer.posY, entityPlayer.posZ);
					
					for(EnumHand hand : EnumHand.values())
					{
						auxiliary = null;
						underbarrel = null;
						laser = null;
						laser1 = null;
						laser2 = null;
						laser3 = null;
						itemStack = entityPlayer.getHeldItem(hand);
						
						if(itemStack.getItem() instanceof ItemGun)
						{
							gun = (ItemGun) itemStack.getItem();
							

							auxiliary = gun.<Auxiliary>getAttachmentCalled(itemStack, EnumAttachmentType.AUXILIARY.getSlot());

							underbarrel = gun.<Underbarrel>getAttachmentCalled(itemStack, EnumAttachmentType.UNDERBARREL.getSlot());
							optic = gun.<Optic>getAttachmentCalled(itemStack, EnumAttachmentType.OPTIC.getSlot());
							rightside = gun.<Ammo>getAttachmentCalled(itemStack, EnumAttachmentType.AMMO.getSlot());
							laser = auxiliary.getLaser();
							laser1 = underbarrel.getLaser();
							laser2 = optic.getLaser();
							laser3 = rightside.getLaser();

							if (laser == null && laser1 != null)
							{
								laser = new Laser(laser1.getR(), laser1.getG(), laser1.getB(), laser1.getMaxRange(), laser1.isBeam(), laser1.isPoint(), laser1.isRangeFinder());
							}
							else if (laser3 != null) {
								laser = new Laser(laser3.getR(), laser3.getG(), laser3.getB(), laser3.getMaxRange(), laser3.isBeam(), laser3.isPoint(), laser3.isRangeFinder());
							}
							if (laser == null && laser2 != null) {
								laser = new Laser(laser2.getR(), laser2.getG(), laser2.getB(), laser2.getMaxRange(), laser2.isBeam(), laser2.isPoint(), laser2.isRangeFinder());
							} else if (laser != null && laser2 != null) {
								laser = new Laser(laser.getR(), laser.getG(), laser.getB(), laser2.getMaxRange(), laser.isBeam(), laser.isPoint(), laser2.isRangeFinder());
							}
						}
						else if(itemStack.getItem() instanceof Auxiliary)
						{
							auxiliary = (Auxiliary) itemStack.getItem();
							laser = auxiliary.getLaser();
						}
						else if(itemStack.getItem() instanceof Underbarrel)
						{
							underbarrel = (Underbarrel) itemStack.getItem();
							laser1 = underbarrel.getLaser();
							if (laser1 != null) {
								laser = new Laser(laser1.getR(), laser1.getG(), laser1.getB(), laser1.getMaxRange(), laser1.isBeam(), laser1.isPoint(), laser1.isRangeFinder());
							}
						}
						else if(itemStack.getItem() instanceof Optic)
						{
							optic = (Optic) itemStack.getItem();
							laser2 = optic.getLaser();
							if (laser2 != null) {
								laser = new Laser(laser2.getR(), laser2.getG(), laser2.getB(), laser2.getMaxRange(), laser2.isBeam(), laser2.isPoint(), laser2.isRangeFinder());
							}
						}

						if(laser != null)
						{
							start = new Vec3d(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight(), entityPlayer.posZ);
							end = entityPlayer.getLookVec().normalize().scale(laser.getMaxRange()).add(start).add(EventHandlerClient.getOffsetForHandRaw(entityPlayer, hand));
							
							resultBlock = EventHandlerClient.findBlockOnPath(world, entityPlayer, start, end);
							resultEntity = EventHandlerClient.findEntityOnPath(world, entityPlayer, start, end);

							//if(resultBlock != null && (resultEntity != null && resultEntity.entityHit != null))
							if(resultBlock != null && resultEntity != null)
							{
								rangeBlockSq = resultBlock.hitVec.distanceTo(start);
								rangeEntitySq = resultEntity.hitVec.distanceTo(start);

								if(rangeBlockSq < rangeEntitySq)
								{
									end = resultBlock.hitVec;
									//System.out.println(rangeBlockSq);

									if (laser.isRangeFinder() && rfHolder) rangeF = (int) rangeBlockSq;
									//title = "/title " + clientPlayer.getName() + " actionbar {\"text\":\"" + range.toString() + "\",\"color\":\"gray\"}";
									//Minecraft.getMinecraft().fontRenderer.drawStringWithShadow("Test", 5, 5, 0xffFFFFFF);
									//ms.commandManager.executeCommand(ms, title);
								}
								else
								{
									end = resultEntity.hitVec;
									//System.out.println(rangeEntitySq);
									if (laser.isRangeFinder() && rfHolder) rangeF = (int) rangeEntitySq;
									//title = "/title " + clientPlayer.getName() + " actionbar {\"text\":\"" + range.toString() + "\",\"color\":\"gray\"}";
									//Minecraft.getMinecraft().fontRenderer.drawStringWithShadow("Test", 5, 5, 0xffFFFFFF);
									//ms.commandManager.executeCommand(ms, title);
								}


								renderPoint = laser.isPoint();

							}
							else if(resultBlock != null)
							{
								end = resultBlock.hitVec;
								//System.out.println(resultBlock.hitVec.distanceTo(start));
								if (laser.isRangeFinder() && rfHolder) rangeF = (int) resultBlock.hitVec.distanceTo(start);
								//title = "/title " + clientPlayer.getName() + " actionbar {\"text\":\"" + range.toString() + "\",\"color\":\"gray\"}";
								//Minecraft.getMinecraft().fontRenderer.drawStringWithShadow("Test", 5, 5, 0xffFFFFFF);
								//ms.commandManager.executeCommand(ms, title);

								renderPoint = laser.isPoint();


							}
							//else if(resultEntity != null && resultEntity.entityHit != null)
							else if(resultEntity != null)
							{
								end = resultEntity.hitVec;
								//System.out.println(resultEntity.hitVec.distanceTo(start));
								if (laser.isRangeFinder() && rfHolder) rangeF = (int) resultEntity.hitVec.distanceTo(start);
								//title = "/title " + clientPlayer.getName() + " actionbar {\"text\":\"" + range.toString() + "\",\"color\":\"gray\"}";
								//Minecraft.getMinecraft().fontRenderer.drawStringWithShadow("Test", 5, 5, 0xffFFFFFF);
								//ms.commandManager.executeCommand(ms, title);

								renderPoint = laser.isPoint();
							}
							
							start = new Vec3d(entityPlayer.posX, entityPlayer.posY + entityPlayer.getEyeHeight() * 0.8F, entityPlayer.posZ).add(EventHandlerClient.getOffsetForHand(entityPlayer, hand));
							
							start = start.subtract(subtract);
							end = end.subtract(subtract);
							
							GlStateManager.disableTexture2D();
							
							if(renderPoint)
							{
								b.begin(7, DefaultVertexFormats.POSITION_COLOR);
								
								b.pos(end.x + 0.05D, end.y + 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y + 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y + 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y + 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								
								b.pos(end.x + 0.05D, end.y - 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y - 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y - 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y - 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								
								b.pos(end.x + 0.05D, end.y - 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y - 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y + 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y + 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								
								b.pos(end.x - 0.05D, end.y - 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y + 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y + 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y - 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								
								b.pos(end.x - 0.05D, end.y - 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y - 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y + 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y + 0.05D, end.z + 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();

								b.pos(end.x - 0.05D, end.y - 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x - 0.05D, end.y + 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y + 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x + 0.05D, end.y - 0.05D, end.z - 0.05D).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								
								tessellator.draw();
							}
							
							if(laser.isBeam())
							{
								b.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);
								
								b.pos(start.x, start.y, start.z).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								b.pos(end.x, end.y, end.z).color(laser.getR(), laser.getG(), laser.getB(), 1F).endVertex();
								
								tessellator.draw();
							}
							
							GlStateManager.enableTexture2D();
						}
					}
				}
			}
		}
	}
	
	public static Vec3d getVectorForRotation(float pitch, float yaw)
	{
		float f = MathHelper.cos(-yaw * 0.017453292F - (float)Math.PI);
		float f1 = MathHelper.sin(-yaw * 0.017453292F - (float)Math.PI);
		float f2 = -MathHelper.cos(-pitch * 0.017453292F);
		float f3 = MathHelper.sin(-pitch * 0.017453292F);
		return new Vec3d(f1 * f2, f3, f * f2);
	}
	
	public static Vec3d getOffsetForHandRaw(EntityPlayer entityPlayer, EnumHand hand)
	{
		Vec3d vec = EventHandlerClient.getVectorForRotation(entityPlayer.rotationPitch, entityPlayer.rotationYaw + 90F);
		
		if(hand == EnumHand.OFF_HAND)
		{
			vec = vec.scale(-1);
		}
		
		return new Vec3d(vec.x, 0, vec.z).normalize().scale(0.4D);
	}
	
	public static Vec3d getOffsetForHand(EntityPlayer entityPlayer, EnumHand hand)
	{
		Vec3d vec = EventHandlerClient.getOffsetForHandRaw(entityPlayer, hand);
		
		return vec.add(EventHandlerClient.getVectorForRotation(entityPlayer.rotationPitch, entityPlayer.rotationYaw).scale(0.4D));
	}
	
	@Nullable
	public static RayTraceResult findBlockOnPath(World world, EntityPlayer entityPlayer, Vec3d start, Vec3d end)
	{
		return world.rayTraceBlocks(start, end, false, false, true);
	}
	
	@Nullable
	public static RayTraceResult findEntityOnPath(World world, EntityPlayer entityPlayer, Vec3d start, Vec3d end)
	{
		RayTraceResult result = null;

		double rangeSq = 0;
		double currentRangeSq;
		for(Entity entity : world.loadedEntityList)
		{
			if (entity != null && (entity.getEntityId() != entityPlayer.getEntityId()) && !(entity instanceof EntityBullet))
			{






				AxisAlignedBB axisalignedbb = entity.getEntityBoundingBox().grow(0.30000001192092896D);
				RayTraceResult result1 = axisalignedbb.calculateIntercept(start, end);

				if (result1 != null)
				{
					currentRangeSq = start.squareDistanceTo(result1.hitVec);
					if (currentRangeSq < rangeSq || result == null)
					{
						result = result1;

						rangeSq = currentRangeSq;
					}
				}
			}
		}
		return result;
	}
	
	public static float calculateFov(float zoom, float fov)
	{
		return (float) Math.atan(Math.tan(fov) / zoom);
	}
	
	public static class ModelRendererTransformationHelper
	{
		private float rotationPointX;
		private float rotationPointY;
		private float rotationPointZ;
		
		private float rotateAngleX;
		private float rotateAngleY;
		private float rotateAngleZ;
		
		public ModelRendererTransformationHelper set(float rotationPointX,
				float rotationPointY, float rotationPointZ, float rotateAngleX,
				float rotateAngleY, float rotateAngleZ)
		{
			this.rotationPointX = rotationPointX;
			this.rotationPointY = rotationPointY;
			this.rotationPointZ = rotationPointZ;
			this.rotateAngleX = rotateAngleX;
			this.rotateAngleY = rotateAngleY;
			this.rotateAngleZ = rotateAngleZ;
			
			return this;
		}
		
		public ModelRendererTransformationHelper transform(EntityPlayer entityPlayer, ModelRenderer model)
		{
			float temp;
			
			temp = model.rotationPointX;
			model.rotationPointX = this.getRotationPointX();
			this.rotationPointX = temp;
			
			temp = model.rotationPointY;
			model.rotationPointY = this.getRotationPointY();
			this.rotationPointY = temp;
			
			temp = model.rotationPointZ;
			model.rotationPointZ = this.getRotationPointZ();
			this.rotationPointZ = temp;
			
			temp = model.rotateAngleX;
			model.rotateAngleX = this.getRotateAngleX();
			this.rotateAngleX = temp;
			
			temp = model.rotateAngleY;
			model.rotateAngleY = this.getRotateAngleY();
			this.rotateAngleY = temp;
			
			temp = model.rotateAngleZ;
			model.rotateAngleZ = this.getRotateAngleZ();
			this.rotateAngleZ = temp;
			
			return this;
		}
		
		public ModelRendererTransformationHelper render(EntityPlayer entityPlayer, ModelRenderer model, float f)
		{
			model.renderWithRotation(f);
			return this;
		}
		
		public ModelRendererTransformationHelper transformAndRender(EntityPlayer entityPlayer, ModelRenderer model, float f)
		{
			this.transform(entityPlayer, model);
			this.render(entityPlayer, model, f);
			this.transform(entityPlayer, model);
			return this;
		}
		
		public float getRotationPointX() {
			return this.rotationPointX;
		}
		public float getRotationPointY() {
			return this.rotationPointY;
		}
		public float getRotationPointZ() {
			return this.rotationPointZ;
		}
		public float getRotateAngleX() {
			return this.rotateAngleX;
		}
		public float getRotateAngleY() {
			return this.rotateAngleY;
		}
		public float getRotateAngleZ() {
			return this.rotateAngleZ;
		}
	}
}
