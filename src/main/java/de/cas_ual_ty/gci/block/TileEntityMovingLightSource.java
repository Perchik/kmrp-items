package de.cas_ual_ty.gci.block;



import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Iterator;
import java.util.List;

/**
 * @author jabelar
 *
 */
public class TileEntityMovingLightSource extends TileEntity implements ITickable {
    public EntityLivingBase theEntityLiving;
    protected boolean shouldDie = false;
    protected int deathTimer = 3; // number of ticks after entityLiving moves away

    public TileEntityMovingLightSource() {
        // after constructing the tile entity instance, remember to call the setentityLiving() method.
//        // DEBUG
//        System.out.println("Constructing");
    }

    @Override
    public void update() {
//    	// DEBUG
//    	System.out.println("Tile entity for entityLiving = "+theEntityLiving+" still ticking");

        // check if already dying
        if (shouldDie) {
//			// DEBUG
//			System.out.println("Should die = "+shouldDie+" with deathTimer = "+deathTimer);
            if (deathTimer > 0) {
                deathTimer--;
                return;
            } else {
                world.setBlockToAir(getPos());
            }
        }

        Block blockAtLocation = world.getBlockState(getPos()).getBlock();

        // clean up in case the entityLiving disappears (teleports, dies, logs out, etc.)
        if (theEntityLiving == null) {
//        	// DEBUG
//        	System.out.println("Setting block to air because entityLiving is null");
            if (blockAtLocation instanceof BlockMovingLightSource) {
                shouldDie = true;
            }
            return;
        }

        // clean up in case the entityLiving disappears (teleports, dies, logs out, etc.)
        if (theEntityLiving.isDead) {
//        	// DEBUG
//        	System.out.println("Setting block to air because entityLiving is null");
            if (blockAtLocation instanceof BlockMovingLightSource) {
                shouldDie = true;
            }
            return;
        }
        // check if entityLiving has moved away from the tile entity or no longer holding light
        // emmitting item set block to air
        double distanceSquared = getDistanceSq(theEntityLiving.posX, theEntityLiving.posY, theEntityLiving.posZ);
        if (distanceSquared > 5.0D)
        {
//        	// DEBUG
//         	System.out.println("Setting block to air because entityLiving moved away, with distance squared = "+distanceSquared+" from entityLiving at position = "+theEntityLiving.getPosition());
            if (blockAtLocation instanceof BlockMovingLightSource)
            {
//            	// DEBUG
//            	System.out.println("Comfirmed that there is moving light source there");
                shouldDie = true;
            }
        }
        // handle case where entityLiving no longer holding light emitting item
        // and also not on fire
        if (! theEntityLiving.isBurning())
        {
//        	// DEBUG
//       	System.out.println("theEntityLiving is not burning");
            if (! BlockMovingLightSource.isHoldingLightItem(theEntityLiving))
            {
//	        	// DEBUG
//	        	System.out.println("Setting block to air because entityLiving ("+theEntityLiving+") is no longer holding light emmitting item and burning state = "+theEntityLiving.isBurning());
                if (world.getBlockState(getPos()).getBlock() instanceof BlockMovingLightSource)
                {
                    //            	// DEBUG
//                                	System.out.println("Comfirmed that there is moving light source there");
                    shouldDie = true;
                }
            }
            else
            {
                // handle the case where the light-emitting item has changed so light level needs to be adjusted
                if (blockAtLocation != BlockMovingLightSource.lightBlockToPlace(theEntityLiving))
                {
                    //	    		// DEBUG
//                    	    		System.out.println("theEntityLiving = "+theEntityLiving+" and tile entity pos = "+getPos());
                    // replace with proper block
                    shouldDie = true;
                }

            }
        }


    }
    public void setEntityLiving(EntityLivingBase parEntityLiving)
    {
//    	// DEBUG
//    	System.out.println("Setting the entity living to "+parEntityLiving);
        theEntityLiving = parEntityLiving;
    }

    public EntityLivingBase getEntityLiving()
    {
        return theEntityLiving;
    }

    public static EntityLivingBase getClosestEntityLiving(World parWorld, BlockPos parPos, double parMaxDistance)
    {
        if (parMaxDistance <= 0.0D)
        {
            return null;
        }

        EntityLivingBase closestLiving = null;
        double distanceSq = parMaxDistance*parMaxDistance;
        AxisAlignedBB aabb = new AxisAlignedBB(
                parPos.getX()-parMaxDistance,
                parPos.getY()-parMaxDistance,
                parPos.getZ()-parMaxDistance,
                parPos.getX()+parMaxDistance,
                parPos.getY()+parMaxDistance,
                parPos.getZ()+parMaxDistance
        );
        List<EntityLivingBase> listEntitiesInRange = parWorld.getEntitiesWithinAABB(EntityLivingBase.class, aabb);
        Iterator<EntityLivingBase> iterator = listEntitiesInRange.iterator();
        while (iterator.hasNext())
        {
            EntityLivingBase next = iterator.next();
            if (getDistanceSq(next.getPosition(), parPos) < distanceSq)
            {
                closestLiving = next;
            }
        }
        return closestLiving;
    }
    protected static double getDistanceSq(BlockPos parPos1, BlockPos parPos2)
    {
        return (  (parPos1.getX()-parPos2.getX())*(parPos1.getX()-parPos2.getX())
                + (parPos1.getY()-parPos2.getY())*(parPos1.getY()-parPos2.getY())
                + (parPos1.getZ()-parPos2.getZ())*(parPos1.getZ()-parPos2.getZ()));
    }
    @Override
    public void setPos(BlockPos posIn)
    {
        pos = posIn.toImmutable();
        setEntityLiving(getClosestEntityLiving(world, pos, 2.0D));
    }
}