package de.cas_ual_ty.gci.inventory.capabilities;

import de.cas_ual_ty.gci.inventory.CustomInventory;

public interface ICAPCustomInventory {

    public void copyInventory(ICAPCustomInventory inventory);
    public CustomInventory getInventory();
}