package de.cas_ual_ty.gci.inventory.bigcontainer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import javax.annotation.Nullable;

public class ContainerBigCont extends Container {

    //Немного кода из ванилы
    //private static final EntityEquipmentSlot[] VALID_EQUIPMENT_SLOTS = new EntityEquipmentSlot[] {EntityEquipmentSlot.HEAD, EntityEquipmentSlot.CHEST, EntityEquipmentSlot.LEGS, EntityEquipmentSlot.FEET};
    private final EntityPlayer thePlayer;
    private final ItemStack container;

    /**
     * Конструктор
     * @param playerInventory Инвентарь игрока
     * @param cInventory Кастомный инвентарь
     * @param player Игрок
     */
    public ContainerBigCont(InventoryPlayer playerInventory, ContainerBigC cInventory, EntityPlayer player) {

        this.thePlayer = player;
        this.container = player.getHeldItemMainhand();
        //Добавляем 8 кастомных слотов. Аргументы: игрок, инвентарь к которому они относятся, индекс слота, х координата, у координата
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 0, 8, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 1, 26, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 2, 44, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 3, 62, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 4, 80, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 5, 98, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 6, 116, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 7, 134, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 8, 152, 41));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 9, 8, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 10, 26, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 11, 44, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 12, 62, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 13, 80, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 14, 98, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 15, 116, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 16, 134, 59));
        this.addSlotToContainer(new StandartSlotCBig(player, cInventory, 17, 152, 59));

        //Все что ниже можно взять из net.minecraft.inventory.ContainerPlayer;
        //Добавляем ванильные слоты для брони
/*        for (int k = 0; k < 4; ++k){
            final EntityEquipmentSlot entityequipmentslot = VALID_EQUIPMENT_SLOTS[k];
            this.addSlotToContainer(new Slot(playerInventory, 36 + (3 - k), 8, 8 + k * 18){

                public int getSlotStackLimit(){
                    return 1;
                }

                public boolean isItemValid(ItemStack stack){
                    return stack.getItem().isValidArmor(stack, entityequipmentslot, thePlayer);
                }

                public boolean canTakeStack(EntityPlayer playerIn){
                    ItemStack itemstack = this.getStack();
                    return !itemstack.isEmpty() && !playerIn.isCreative() && EnchantmentHelper.hasBindingCurse(itemstack) ? false : super.canTakeStack(playerIn);
                }
                @Nullable
                @SideOnly(Side.CLIENT)
                public String getSlotTexture(){
                    return ItemArmor.EMPTY_SLOT_NAMES[entityequipmentslot.getIndex()];
                }
            });
        }*/

        //Добавляем 27 ванильных слотов инвентаря игрока
        int si;
        int sj;
        for (si = 0; si < 3; ++si)
            for (sj = 0; sj < 9; ++sj)
                this.addSlotToContainer(new Slot(player.inventory, sj + (si + 1) * 9, 8 + sj * 18, 84 + si * 18));
        for (si = 0; si < 9; ++si)
            this.addSlotToContainer(new Slot(player.inventory, si, 8 + si * 18, 142));

    }

    /**
     * Этот метод срабатывает когда игрок зажимает Шифт и кликает на слот с целью переместить предмет.
     * Здесь мы должны задать откуда и куда будут перемещаться предметы из слота по которому кликнули
     * @param index Индекс слота, на который кликнул игрок
     */
    @Nullable
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = (Slot) this.inventorySlots.get(index);
        if (slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            //запретим перемещать сам контейнер шифт-кликом
            if (itemstack1 == playerIn.getHeldItemMainhand()){
                return ItemStack.EMPTY;
            }
            if (index < 9) {
                if (!this.mergeItemStack(itemstack1, 9, this.inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
                slot.onSlotChange(itemstack1, itemstack);
            } else if (!this.mergeItemStack(itemstack1, 0, 9, false)) {
                if (index < 9 + 27) {
                    if (!this.mergeItemStack(itemstack1, 9 + 27, this.inventorySlots.size(), true)) {
                        return ItemStack.EMPTY;
                    }
                } else {
                    if (!this.mergeItemStack(itemstack1, 9, 9 + 27, false)) {
                        return ItemStack.EMPTY;
                    }
                }
                return ItemStack.EMPTY;
            }
            if (itemstack1.getCount() == 0) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
            if (itemstack1.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, itemstack1);
        }
        return itemstack;
    }

    //Запретить перетаскивать контейнер из основной руки
    @Override
    public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player)
    {

            try {
                ItemStack itemstackMain = player.getHeldItemMainhand();
                /*int slotMainID = player.inventory.getSlotFor(itemstackMain);*/

                if (clickTypeIn == ClickType.SWAP) return ItemStack.EMPTY;
                if (clickTypeIn != ClickType.QUICK_CRAFT) {
                    if (getSlot(slotId).getStack() == itemstackMain) return ItemStack.EMPTY;
                }

                return super.slotClick(slotId, dragType, clickTypeIn, player);
            } catch (ArrayIndexOutOfBoundsException ae){
//                System.out.println(ae);
                return ItemStack.EMPTY;
            }


    }

    /**
     * Может ли игрок взаимодействовать с инвентарем?
     */
    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }


}
